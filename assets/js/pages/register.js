$(document).ready(function() {
	var alertbox = new Alert();
	var account = new Account();
	var validate = new AccountValidation();
	var ismobile = $("body").data("mobile");
	
	// handler to check or validate each input field
	$(".input-field > input").on("blur", function() {
		var element = $(this);
		var id = element.attr("id");
		
		validate.element = element;
		validate.label = getFieldName(element);
		validate.check();
		
		switch(id) {
		case "Username":
		case "Referral":
		case "Captcha":
			element.attr("data-validate", "false");
			if( element.attr("data-validate") == "false" ) {
				element.next("i").css("opacity", "0");
			} else {
				element.next("i").css("opacity", "1");
			}
			break;
		}
	});
	
	// check availability
	$("#CheckUsername").on("click", function() {
		var element = ismobile == true ? $(this).prev().children("input") : $(this).parent().prev().find("input");
		var value = element.val();

		checkAvailability(element, value);
	});
	
	// show and hide password
	$(".btn-pwd-visibility").on("click", function() {
		var pwdfield = $(".pwdinput");
		var type = pwdfield.attr("type") == "password" ? "text" : "password";
		var buttontext = type == "password" ? button.show : button.hide;
		
		$(this).text(buttontext);
		pwdfield.attr("type", type);
	});
	
	// handler to reload captcha
	$(".btn-reload-captcha").on("click", function() {
		resetCaptcha();
	});
	
	// register button
	$("#RegisterButton").on("click", function(e) {
		e.preventDefault();
		// disabled register button preventing from too many clicks
		var btnReg = $(this);
		btnReg.attr("disabled", "disabled");
		$(".input-field > input").blur();
		
		$('#RegisterForm').find('input').each(function(){
			var element = $(this);
			var id = element.attr("id");
			var value = element.val();
			
			if( $(this).prop('required') ){
				validate.element = element;
				validate.label = getFieldName(element);
				validate.check();
			}
			
			if( value != "" ) {
				switch(id) {
				case "Username":
				case "Referral":
				case "Captcha":
					checkAvailability(element, value);
					break;
				}
			}
		});
		
		if( validate.isError() == false ) {
			account.username = $("#Username").val();
			account.password = $("#Password").val();
			account.cpassword = $("#CPassword").val();
			account.firstname = $("#FirstName").val();
			account.countrycode = $('#CountryCode').val();
			account.mobilenumber = $('#MobileNumber').val();
			account.referral = $("#Referral").val();
			account.captcha = $("#Captcha").val();
			account.token = $("#Token").val();
			account.devicetoken = ismobile == true ? deviceToken : "";
			account.lang = $("html").attr("lang");
			account.register();
			
			alertbox.code = account.response.code;
			alertbox.message = account.response.data.message;
			alertbox.set();
			if( account.response.success == true ) {
				var countdown = 3;
				var url = account.response.data.redirect;
				
				account.emptyform("register");
				alertbox.redirect(countdown, url);
			} else {
				alertbox.remove();
				btnReg.removeAttr("disabled");
			}
		} else {
			validate.setFocus();
			btnReg.removeAttr("disabled");
		}
	});
	
	/*** FUNCTIONS ***/
	function checkAvailability(element, value) {
		var id = element.attr("id");
		
		switch(id) {
		case "Username":
			account.username = value;
			account.checkUsername();
			break;
		case "Referral":
			account.referral = value;
			account.checkReferral();
			break;
		case "Captcha":
			account.captcha = value;
			account.type = "register";
			account.checkCaptcha();
			break;
		}
		
		validate.element = element;
		validate.label = getFieldName(element);
		if( account.response.success == false ) {
			validate.message = account.response.data.message;
			validate.setError();
			element.attr("data-validate", "false");
			if( id == "Captcha" ) {
				resetCaptcha(false);
			}
		} else {
			validate.setSuccess();
			element.attr("data-validate", "true");
			if( id == "Captcha" ) {
				resetCaptcha();
				element.next("i").css("opacity", "0");
			}
		}
	}
	
	function getFieldName(element) {
		var fieldname;
		var label1 = element.parent().prev("label");
		var label2 = element.parent().parent().prev("label");
		
		if( label1.length > 0 ) {
			fieldname = label1.text().replace("*", "").trim();
		} else {
			fieldname = label2.text().replace("*", "").trim();
		}
		
		return fieldname;
	}
	
	function resetCaptcha(isTriggered) {
		if( isTriggered == true ) {
			$("#Captcha").closest(".textfield-wrap").removeClass("validation-pass").removeClass("validation-error");
		}
		$("#imgCaptcha").attr('src', '/captcha?p=register&cache=' + new Date().getTime());
		$("#Captcha").val("");
	}

});
